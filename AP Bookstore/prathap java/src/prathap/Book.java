package prathap;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import net.proteanit.sql.DbUtils;
import prathap.MysqlConnection;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JTable;
import javax.swing.JScrollPane;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.*;
import javax.swing.*;

public class Book extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Book frame = new Book();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Connection connection = null;
	public Book() {
		getContentPane().setLayout(null);
		connection = MysqlConnection.Dbconnecter();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 665, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblBooks = new JLabel("Books");
		lblBooks.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblBooks.setBounds(177, 13, 109, 31);
		contentPane.add(lblBooks);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			
			
			public void keyReleased(KeyEvent arg0 ){
				try {
					
					String query = "select * from book where bookname =? ";
					PreparedStatement pst = connection.prepareStatement(query);
					
					pst.setString(1, textField.getText());
					
					ResultSet rs = pst.executeQuery();
					
					table.setModel(DbUtils.resultSetToTableModel(rs));
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		
		textField.setBounds(154, 57, 116, 22);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblBookName = new JLabel("Book name");
		lblBookName.setBounds(32, 60, 87, 16);
		contentPane.add(lblBookName);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(23, 104, 463, 193);
		contentPane.add(scrollPane);
		
		table = new JTable();
		scrollPane.setViewportView(table);
	}
}
