import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.Font;

public class Admin extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Admin frame = new Admin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Admin() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Update");
		btnNewButton.setBounds(34, 75, 126, 51);
		contentPane.add(btnNewButton);
		
		JButton btnTransication = new JButton("Transication");
		btnTransication.setBounds(211, 79, 139, 43);
		contentPane.add(btnTransication);
		
		JButton btnSearch = new JButton("Search");
		btnSearch.setBounds(238, 143, 112, 51);
		contentPane.add(btnSearch);
		
		JButton btnNewButton_1 = new JButton("New button");
		btnNewButton_1.setBounds(44, 143, 112, 51);
		contentPane.add(btnNewButton_1);
		
		JButton btnAdmin = new JButton("Admin");
		btnAdmin.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		btnAdmin.setBounds(131, 11, 161, 23);
		contentPane.add(btnAdmin);
	}

}
