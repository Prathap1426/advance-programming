

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class libmain {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					libmain window = new libmain();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public libmain() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblBookStore = new JLabel("Book Store");
		lblBookStore.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblBookStore.setBounds(141, 11, 151, 36);
		frame.getContentPane().add(lblBookStore);
		
		JButton btnNewButton_2 = new JButton("create new User");
		btnNewButton_2.setBounds(285, 49, 139, 30);
		frame.getContentPane().add(btnNewButton_2);
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 101, 174, 107);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblAdmin = new JLabel("Admin");
		lblAdmin.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblAdmin.setBounds(33, 0, 46, 14);
		panel.add(lblAdmin);
		
		JLabel lblAdminname = new JLabel("AdminName");
		lblAdminname.setBounds(0, 30, 79, 14);
		panel.add(lblAdminname);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(0, 55, 79, 14);
		panel.add(lblPassword);
		
		textField = new JTextField();
		textField.setBounds(78, 27, 86, 20);
		panel.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(78, 52, 86, 20);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnSignin = new JButton("SignIn");
		btnSignin.setBounds(40, 80, 89, 23);
		panel.add(btnSignin);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(208, 101, 204, 105);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblUser = new JLabel("User");
		lblUser.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUser.setBounds(78, 0, 46, 14);
		panel_1.add(lblUser);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setBounds(10, 26, 88, 14);
		panel_1.add(lblUsername);
		
		JLabel lblPassword_1 = new JLabel("Password");
		lblPassword_1.setBounds(10, 51, 88, 14);
		panel_1.add(lblPassword_1);
		
		textField_2 = new JTextField();
		textField_2.setBounds(108, 25, 86, 20);
		panel_1.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(108, 48, 86, 20);
		panel_1.add(textField_3);
		textField_3.setColumns(10);
		
		JButton btnSignin_1 = new JButton("SignIn");
		btnSignin_1.setBounds(58, 79, 89, 23);
		panel_1.add(btnSignin_1);
	}
}
