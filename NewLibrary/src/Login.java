import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextArea;
import java.awt.SystemColor;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login frame = new Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 446, 355);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JTextArea textArea = new JTextArea("Admin Login");
		textArea.setForeground(Color.BLACK);
		textArea.setFont(new Font("Monospaced", Font.PLAIN, 18));
		textArea.setBackground(SystemColor.menu);
		textArea.setBounds(117, 0, 168, 35);
		contentPane.add(textArea);
		
		JTextArea textArea_1 = new JTextArea("User Name :");
		textArea_1.setBackground(SystemColor.menu);
		textArea_1.setBounds(10, 54, 97, 22);
		contentPane.add(textArea_1);
		
		JTextArea textArea_2 = new JTextArea("User Name :");
		textArea_2.setBackground(SystemColor.menu);
		textArea_2.setBounds(10, 85, 97, 22);
		contentPane.add(textArea_2);
		
		textField = new JTextField();
		textField.setColumns(10);
		textField.setBounds(109, 56, 176, 20);
		contentPane.add(textField);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(109, 87, 176, 20);
		contentPane.add(passwordField);
		
		JButton button = new JButton("Back");
		button.setBounds(109, 118, 89, 23);
		contentPane.add(button);
		
		JButton button_1 = new JButton("Sign In");
		button_1.setBounds(208, 118, 89, 23);
		contentPane.add(button_1);
	}

}
