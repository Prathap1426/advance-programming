

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class Newuser extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Newuser frame = new Newuser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Newuser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewUser = new JLabel("New User");
		lblNewUser.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 18));
		lblNewUser.setBounds(114, 11, 215, 37);
		contentPane.add(lblNewUser);
		
		JLabel lblUsername = new JLabel("UserName");
		lblUsername.setBounds(29, 89, 79, 14);
		contentPane.add(lblUsername);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(29, 114, 46, 14);
		contentPane.add(lblEmail);
		
		JLabel lblMobile = new JLabel("Mobile");
		lblMobile.setBounds(29, 139, 46, 14);
		contentPane.add(lblMobile);
		
		JLabel lblAddress = new JLabel("Address");
		lblAddress.setBounds(29, 164, 46, 14);
		contentPane.add(lblAddress);
		
		textField = new JTextField();
		textField.setBounds(114, 86, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(114, 111, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(114, 136, 86, 20);
		contentPane.add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(114, 161, 86, 20);
		contentPane.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(29, 189, 46, 14);
		contentPane.add(lblPassword);
		
		textField_4 = new JTextField();
		textField_4.setBounds(114, 186, 86, 20);
		contentPane.add(textField_4);
		textField_4.setColumns(10);
		
		JButton btnSignup = new JButton("Signup");
		btnSignup.setBounds(75, 217, 89, 23);
		contentPane.add(btnSignup);
	}
}
